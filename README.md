# FuzzyCompare


# This is the algorithm I used:

1.  Read and Parse the file to get address list.
2.  Separate the address list into cities, streets, zip codes etc. (we can implement multiple implementations based on countries but here I have given examples using us addresses).
3.  I loop through each address items and produce a parsed address list as above.
4.  Then I use levenstein distance to do fuzzy string matching on a separate address entities along with the below comparison logic and order.
5.  Then out of the this list I create a FuzzyDistinct address list (e.g a master list). For this I use 2 sets of comparisons.
     * If an entity or organization name is given and then either the (PO box + State + Zip/City + country) or (unit number + street address + State + Zip/City + country)  [ this is because an entity may have both zip code and street address]
     * if an entity is not given then we just compare all other address components.
     * I have used the following order when using fuzzy string equality on entities and street address only (because assuming a proper input form like a website form the zip code, state and country are likely to be strictly categorized and can be compared directly)
     * for Entities and street address. I have the following order within - to do equality, contains and levenstein distance ceiled based on a paremeter.
     * This levenstein ceil constant is set to 8 and can be adjusted based on the data or even subset of data.

# Alternatives

1.  idx inverse frequency terms (I have implemented this algorithm for a twitter search engine I built using nodeJS,
I can provide source code if needed)
2.  use street address web services provided by postal offices