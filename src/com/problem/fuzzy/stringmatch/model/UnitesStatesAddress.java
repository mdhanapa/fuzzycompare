package com.problem.fuzzy.stringmatch.model;

import com.problem.fuzzy.stringmatch.util.Util;
import lombok.Data;

@Data
public class UnitesStatesAddress implements Address {

    private static final int MATCHING_THRESHOLD = 8;

    private String entity;
    private String state="";
    private String zip="";
    private String city="";
    private String street="";
    private String poNumber;
    private String unitNumber;
    private String country="";


    private int levenshteinDistance(String s1, String s2){

        if (s1 == null || s2 == null) return 0;
        if (s1.length() == 0) return s2.length();
        if (s2.length() == 0) return s1.length();
        int l1 = s1.length();
        int l2 = s2.length();

        int memoize[][] = new int[l1+1][l2+1];

        for (int i=0; i<=l1; i++)
        {
            for (int j=0; j<=l2; j++)
            {
                if (i==0)
                    memoize[i][j] = j;
                else if (j==0)
                    memoize[i][j] = i;
                else if (s1.charAt(i-1) == s2.charAt(j-1))
                    memoize[i][j] = memoize[i-1][j-1];
                else
                    memoize[i][j] = 1 + Util.min(memoize[i][j-1],
                            memoize[i-1][j],
                            memoize[i-1][j-1]);
            }
        }

        //System.out.println("ld("+s1+","+s2+") = "+ memoize[l1][l2]);
        return memoize[l1][l2];
    }

    @Override
    public boolean equals(Object address) {
        if(! (address  instanceof UnitesStatesAddress)) return false;

        UnitesStatesAddress otherAddress = (UnitesStatesAddress) address;
        boolean isSame = false;
        boolean isSameEntity = isSameEntity(entity, otherAddress.getEntity());

        if(isSameEntity){
            isSame = isSameState(state, otherAddress.getState()) &&
                    isSameCountry(country, otherAddress.getCountry()) &&
                    (isSameZipCode(zip, otherAddress.getZip()) ||
                            isSameCity(city, otherAddress.getCity()));
        }
        else {
            isSame = isSameState(state, otherAddress.getState()) &&
                    isSameCountry(country, otherAddress.getCountry()) &&
                    isSamePONumber(poNumber, otherAddress.getPoNumber()) &&
                    (isSameZipCode(zip, otherAddress.getZip()) ||
                            isSameCity(city, otherAddress.getCity())) &&
                    isSameUnitNumber(unitNumber, otherAddress.getUnitNumber()) &&
                    isSameStreetAddress(street, otherAddress.getStreet());
        }


        //uncomment to see each address compared to other address to arrive at a unique address list;
        /*
        System.out.println();
        System.out.println(this);
        if(!isSame) {
            System.out.println("is different from");
        }
        else {
            System.out.println("is same as");
        }
        System.out.println(otherAddress);
        System.out.println();
        */
        return isSame;
    }

    private boolean isSameCountry(String country1, String country2){
        return country1.equalsIgnoreCase(country2);
    }

    private boolean isSameState(String state1, String state2){
        return state1.equalsIgnoreCase(state1);
    }

    private boolean isSameCity(String city1, String city2){
        return city1.equalsIgnoreCase(city2);
    }

    private boolean isSamePONumber(String poNumber1, String poNumber2){

        if(poNumber1 == null && poNumber2 == null) return true;
        if(poNumber1 == null || poNumber2 == null) return false;
        else if(poNumber1.equalsIgnoreCase(poNumber2)) return true;
        else return false;
    }

    private boolean isSameUnitNumber(String unitNumber1, String unitNumber2){

        if(unitNumber1 == null && unitNumber2 == null) return true;
        if(unitNumber1 == null || unitNumber2 == null) return false;
        else if(unitNumber1.equalsIgnoreCase(unitNumber2)) return true;
        else return false;
    }

    private boolean isSameEntity(String entity1, String entity2){

        if(entity1 == null && entity2 == null) return true;
        if(entity1 == null || entity2 == null) return false;
        entity1 = entity1.toLowerCase();
        entity2 = entity2.toLowerCase();
        if(entity1.equalsIgnoreCase(entity2)) return true;
        else if(entity1.contains(entity2) || entity2.contains(entity1)) return true;
        return levenshteinDistance(entity1, entity2) < MATCHING_THRESHOLD;
    }

    private boolean isSameStreetAddress(String street1, String street2){

        if(street1 == null && street2 == null) return true;
        if(street1 == null || street2 == null) return false;
        street1 = street1.toLowerCase();
        street2 = street2.toLowerCase();
        if(street1.equalsIgnoreCase(street2)) return true;
        else if(street1.contains(street2) || street2.contains(street1)) return true;

        return levenshteinDistance(street1, street2) < MATCHING_THRESHOLD;
    }

    private boolean isSameZipCode(String zipCode1, String zipCode2){
        return zipCode1.equalsIgnoreCase(zipCode2);
    }

    @Override
    public String toString(){
        StringBuilder  output = new StringBuilder();

        if(entity!=null && entity.length()!=0) output.append("" + entity + "; ");
        if(poNumber != null && poNumber.length()!=0){
            output.append("PO BOX: " + poNumber) ;
        }
        else {
            output.append("" + (unitNumber + " " + street).trim());
        }
        if(city!=null && city.length()!=0) output.append("; " + city);
        if(state!=null && state.length()!=0) output.append("; " + state);
        if(zip!=null && zip.length()!=0) output.append("; " + zip);
        if(country!=null && country.length()!=0) output.append("; " + country);

        return output.toString();
    }

}
