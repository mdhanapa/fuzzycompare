package com.problem.fuzzy.stringmatch.model;

import lombok.Data;

@Data
public class CSVRecord {
    String inputName;
    String inputAddress;
    UnitesStatesAddress outputAddress;
    String outputName;
}
