package com.problem.fuzzy.stringmatch.util;

public class Util {

    public static int min(int a, int b, int c){
        int minFirstTwo = Math.min(a,b);
        return Math.min(c, minFirstTwo);
    }

    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
}
