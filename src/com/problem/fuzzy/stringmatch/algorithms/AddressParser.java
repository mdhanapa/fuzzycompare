package com.problem.fuzzy.stringmatch.algorithms;

import com.problem.fuzzy.stringmatch.model.UnitesStatesAddress;
import com.problem.fuzzy.stringmatch.util.Util;

public class AddressParser {
    private String address;

    public AddressParser(String address){
        this.address = address;
    }

    public UnitesStatesAddress parse(){
        UnitesStatesAddress unitesStatesAddress = new UnitesStatesAddress();

        String[] parts = address.split(",");

        int index = 0;
        for(String part:parts){
            index++;
            part = part.trim().toLowerCase();
            String[] subparts = part.split("\\s+");
            if(subparts.length > 1 && Util.isNumeric(subparts[0])){
                unitesStatesAddress.setUnitNumber(subparts[0].trim());
                unitesStatesAddress.setStreet(part.substring(subparts[0].length()).trim());
            }
            else if(part.startsWith("po box") || part.startsWith("P.O. BOX")){
                unitesStatesAddress.setPoNumber(subparts[2].trim());
            }
            else if(index==1){
                unitesStatesAddress.setEntity(part.trim());
            }

        }

        unitesStatesAddress.setCountry(parts[parts.length-1].trim());
        unitesStatesAddress.setZip(parts[parts.length-2].trim());
        unitesStatesAddress.setState(parts[parts.length-3].trim());
        unitesStatesAddress.setCity(parts[parts.length-4].trim());

        //System.out.println("\n" + unitesStatesAddress.toString() + "\n");

        return unitesStatesAddress;

    }
}
