package com.problem.fuzzy.stringmatch.algorithms;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.problem.fuzzy.stringmatch.model.CSVRecord;
import com.problem.fuzzy.stringmatch.model.UnitesStatesAddress;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FuzzyAddressResolver {

    private static final String fileColumnSeperator = ",";


    public FuzzyAddressResolver(){

    }

    public List<CSVRecord> readFile(String inputFile) {
        List<CSVRecord> csv = new LinkedList<>();


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile))) {
            CSVReader csvReader = new CSVReader(bufferedReader);
            List<String[]> list = new ArrayList<>();
            String[] csvline;
            int records = 0;
            while ((csvline = csvReader.readNext()) != null ) {
                records++;
                if (csvline.length <1) continue;
                if(csvline[0].trim().length() == 0 ) continue;
                if(csvline[1].trim().length() == 0 ) continue;

                list.add(csvline);
                //System.out.println("name, address [name= " + csvline[0].trim() + " , address=" + csvline[1].trim() + "]");

                CSVRecord csvRecord = new CSVRecord();
                csvRecord.setInputName(csvline[0].trim());
                csvRecord.setInputAddress(csvline[1].trim());
                csv.add(csvRecord);
            }
            csvReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return csv;
    }

    public List<UnitesStatesAddress> addressParser(List<CSVRecord> csvRecords){
        List<UnitesStatesAddress> usaAddresses = new LinkedList<>();
        for(CSVRecord csvRecord:csvRecords){
            AddressParser addressParser = new AddressParser(csvRecord.getInputAddress());
            UnitesStatesAddress unitesStatesAddress = addressParser.parse();
            usaAddresses.add(unitesStatesAddress);
        }
        return usaAddresses;
    }

    public List<UnitesStatesAddress> getDistinctAddresses(List<UnitesStatesAddress> usaAddresses) {
        List<UnitesStatesAddress>  uniqueAddresses = new LinkedList<>();

        for(UnitesStatesAddress unitesStatesAddress1:usaAddresses){
            UnitesStatesAddress chosen = unitesStatesAddress1;
            int index = 0;
            boolean remove = false;
            for(UnitesStatesAddress unitesStatesAddress2:uniqueAddresses){
                if(unitesStatesAddress1.equals(unitesStatesAddress2)){
                    if(unitesStatesAddress2.toString().length() > unitesStatesAddress1.toString().length()){
                        chosen = unitesStatesAddress2;
                    }
                    remove = true;
                    break;
                }
                index++;
            }
            uniqueAddresses.add(index, chosen);
            if(remove==true) uniqueAddresses.remove(index);
        }

        return uniqueAddresses;
    }

    public List<UnitesStatesAddress> getFuzzyMappedAddresses(List<UnitesStatesAddress> usaAddresses, List<UnitesStatesAddress> uniqueAddresses) {
           List<UnitesStatesAddress> csvMappedAddresses = new LinkedList<>();

           for(UnitesStatesAddress address: usaAddresses){
               for(UnitesStatesAddress uniqueAddress: uniqueAddresses){
                   if(address.equals(uniqueAddress)){
                       csvMappedAddresses.add(uniqueAddress);
                   }
               }
           }
           return  csvMappedAddresses;
    }

    public void updateCSVRecords(List<CSVRecord> csv, List<UnitesStatesAddress> fuzzyMappedAddresses) {
        boolean updateName = true;
        String name = "";
        int index = 0;
        int len = fuzzyMappedAddresses.size();
        for(CSVRecord record: csv){
            if(updateName){
                name = record.getInputName();
            }

            UnitesStatesAddress address = fuzzyMappedAddresses.get(index++);
            record.setOutputAddress(address);
            record.setOutputName(name);

            if(index < len && fuzzyMappedAddresses.get(index).equals(address)) {
                updateName =  false;
            }else {
                updateName = true;
            }
        }
    }

    public void writeOutputfile(List<CSVRecord> csv, String s) throws IOException {
        try (
                Writer writer = Files.newBufferedWriter(Paths.get("resources/problem-out.csv"));

                CSVWriter csvWriter = new CSVWriter(writer);
        ) {
            String[] headerRecord = {"InputName", "InputAddress", "OutputName", "OutputAddress"};
            csvWriter.writeNext(headerRecord);
            for(CSVRecord record: csv){
                csvWriter.writeNext(new String[]{record.getInputName(), record.getInputAddress(),
                        record.getOutputName(), record.getOutputAddress().toString()});
            }
        }
    }
}
