package com.problem.fuzzy.stringmatch;

import com.problem.fuzzy.stringmatch.model.CSVRecord;
import com.problem.fuzzy.stringmatch.algorithms.FuzzyAddressResolver;
import com.problem.fuzzy.stringmatch.model.UnitesStatesAddress;

import java.io.*;
import java.util.List;

public class App {

    public static void main(String[] args) throws IOException {

        FuzzyAddressResolver fuzzyAddressResolver = new FuzzyAddressResolver();

        List<CSVRecord> csv = fuzzyAddressResolver.readFile("resources/problem.csv");

        List<UnitesStatesAddress> usaAddresses = fuzzyAddressResolver.addressParser(csv);

        List<UnitesStatesAddress> uniqueAddresses = fuzzyAddressResolver.getDistinctAddresses(usaAddresses);

        List<UnitesStatesAddress> fuzzyMappedAddresses = fuzzyAddressResolver.getFuzzyMappedAddresses(usaAddresses, uniqueAddresses);

        fuzzyAddressResolver.updateCSVRecords(csv, fuzzyMappedAddresses);

        fuzzyAddressResolver.writeOutputfile(csv, "resources/problem-out.csv");
    }

}
